import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const fetchLocationData = async (locationId) => {
    const url = `http://localhost:8100/api/locations/${locationId}`;

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        return data.closet_name;
    } else {
        console.error(response);
        return null;
    }
}

const HatList = (props) => {
    const [hatColumns, setHatColumns] = useState([[], [], []]);

    function HatColumn(props) {
        const [locationNames, setLocationNames] = useState({});

        useEffect(() => {
            const fetchLocationNames = async () => {
                const newLocationNames = {};
                for (const data of props.list) {
                    const locationId = data.location.id;
                    if (!newLocationNames[locationId]) {
                        newLocationNames[locationId] = await fetchLocationData(locationId);
                    }
                }
                setLocationNames(newLocationNames);
            }
            fetchLocationNames();
        }, [props.list]);

        const handleDelete = async (e) => {


            const url = `http://localhost:8090${e.target.value}`
            console.log(e.target.value)

            const fetchConfigs = {
                method: "Delete",
                headers: {
                    "Content-Type": "application/json"
                }
            }

            const resp = await fetch(url, fetchConfigs)
            const data = await resp.json()

            if (resp.ok) {
                fetchData()
            }

        }

        return (
            <div className="col">
                {props.list.map(data => {
                    const hat = data;

                    return (
                        <div key={hat.href} className="card mb-3 shadow">
                            <img src={hat.picture} className="card-img-top" />
                            <div className="card-body">
                                <h5 className="card-title">{hat.style}</h5>
                                <h6 className="card-subtitle mb-2 text-muted">
                                    {hat.fabric}
                                </h6>
                                <p className="card-text">
                                    {hat.color}
                                </p>
                            </div>
                            <div className="card-footer">
                                <div>
                                    {locationNames[data.location.id] && (
                                        <p>{locationNames[data.location.id]}</p>
                                    )}
                                </div>
                                <button value={hat.href} className="btn btn-danger me-2" onClick={handleDelete}>Delete</button>
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                console.log(data)

                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090${hat.href}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const columns = [[], [], []];

                let i = 0;
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        columns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(hatResponse);
                    }
                }
                setHatColumns(columns);
            }
        } catch (e) {
            console.error(e);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <div id='aaammm' className="px-4 py-5 my-5 mt-5 text-center bg-info ">
                <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/hats.png" alt="" width="600" />
                <h1 className="display-5 fw-bold">Wardrobify</h1>
                <div className="col-lg-6 mx-auto">
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Hat</Link>
                    </div>
                </div>
            </div>
            <div className="location">
                <h2>Your Hats</h2>
                <div className="row">
                    {hatColumns.map((hatList, index) => {
                        return (
                            <HatColumn key={index} list={hatList} />
                        );
                    })}
                </div>
            </div>
        </>
    );
}

export default HatList;

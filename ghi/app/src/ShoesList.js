import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const fetchBinData = async (binId) => {
  const url = `http://localhost:8100/api/bins/${binId}`;

  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    return data.closet_name;
  } else {
    console.error(response);
    return null;
  }
}

const ShoesList = (props) =>  {
  const [shoeColumns, setShoeColumns] = useState([[], [], []]);

  function ShoeColumn(props) {
    const [binNames, setBinNames] = useState({});

    useEffect(() => {
      const fetchBinNames = async () => {
        const newBinNames = {};
        for (const data of props.list) {
          const binId = data.bin.id;
          if (!newBinNames[binId]) {
            newBinNames[binId] = await fetchBinData(binId);
          }
        }
        setBinNames(newBinNames);
      }
      fetchBinNames();
    }, [props.list]);

    const handleDelete = async (e) => {


      const url = `http://localhost:8080${e.target.value}`
      console.log(e.target.value)

      const fetchConfigs = {
        method: "Delete",
        headers: {
          "Content-Type": "application/json"
        }
      }

      const resp = await fetch(url, fetchConfigs)
      const data = await resp.json()

      if (resp.ok){
        fetchData()
      }

    }

    return (
      <div className="col">
        {props.list.map(data => {
          const shoe = data;

          return (
            <div key={shoe.href} className="card mb-3 shadow">
              <img src={shoe.img_url} className="card-img-top" />
              <div className="card-body">
                <h5 className="card-title">{shoe.model_name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                  {shoe.manufacturer}
                </h6>
                <p className="card-text">
                  {shoe.color}
                </p>
              </div>
              <div className="card-footer">
                <div>
                {binNames[data.bin.id] && (
                  <p>{binNames[data.bin.id]}</p>
                )}
                </div>
                <button value={shoe.href} className="btn btn-danger me-2" onClick={handleDelete}>Delete</button>
              </div>
            </div>
          );
        })}
      </div>
    );
  }

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        console.log(data)

        const requests = [];
        for (let shoe of data.shoes) {
          const detailUrl = `http://localhost:8080${shoe.href}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);

        const columns = [[], [], []];

        let i = 0;
        for (const shoeResponse of responses) {
          if (shoeResponse.ok) {
            const details = await shoeResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(shoeResponse);
          }
        }
        setShoeColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div id='aaammm' className="px-4 py-5 my-5 mt-5 text-center bg-info ">
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
        <h1 className="display-5 fw-bold">Wardrobify</h1>
        <div className="col-lg-6 mx-auto">
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoe/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Shoe</Link>
          </div>
        </div>
      </div>
      <div className="container">
        <h2>Your Shoes</h2>
        <div className="row">
          {shoeColumns.map((ShoesList, index) => {
            return (
              <ShoeColumn key={index} list={ShoesList} />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default ShoesList;

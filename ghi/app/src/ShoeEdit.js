import React, { useEffect, useState } from 'react';
import logo from './logo.svg'

const EditShoeForm = async (e) => {

    const [bins, setBins] = useState([])
    const url = `http://localhost:8080${e.target.value}`
    console.log(e.target.value)

    const response = await fetch(url)

    const data = await response.json()
    let initialState = {
        manufacturer: data.manufacturer,
        model_name: data.model_name,
        color: data.color,
        img_url: data.img_url,
        bin: data.bin
    }
    const [formData, setFormData] = useState(initialState)


    // const fetchConfigs =
    // {
    //     method: "POST",
    //     body: JSON.stringify(formData),

    //     headers: {
    //         'Content-Type': 'application/json'
    //     }
    // }

    setFormData({
        ...formData,
        [e.target.name]: e.target.value
    })




    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (bins.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    const handleFormChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const shoeUrl = `http://localhost:8080${e.target.value}`
        console.log(formData)
        const fetchConfig =
        {
            method: "PUT",
            body: JSON.stringify(formData),

            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
            console.log(response)
            const newShoe = await response.json()
            console.log(newShoe)
            setFormData(initialState)
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className=" my-5 container">
            <div className="my-5">
                <div className="row">
                    <div className="col col-sm-auto">
                        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src={logo} />
                    </div>
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-shoe-form">
                                    <h1 className="card-title">It's Shoe Time!</h1>
                                    <p className="mb-3">
                                        Please choose which bin you'd like to put your shoe in!
                                    </p>
                                    <div className={spinnerClasses} id="loading-bin-spinner">
                                        <div className="spinner-grow text-secondary" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <div className="mb-3">
                                        <select onChange={handleFormChange} value={formData.bin} name="bin" id="bin" className={dropdownClasses} required>
                                            <option value="">Choose a bin</option>
                                            {bins.map(bin => {
                                                return (
                                                    <option key={bin.id} value={bin.id}>
                                                        {bin.closet_name}
                                                    </option>
                                                )
                                            })}
                                        </select>
                                    </div>
                                    <p className="mb-3">
                                        Now, tell us about the shoe!
                                    </p>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleFormChange} value={formData.manufacturer} required placeholder="Manufacturer" type="text" id="manufacturer" name="manufacturer" className="form-control" />
                                            <label htmlFor="manufacturer">Manufacturer</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleFormChange} required value={formData.model_name} placeholder="Model Name" type="text" id="model_name" name="model_name" className="form-control" />
                                            <label htmlFor="model_name"> Model Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleFormChange} required value={formData.color} placeholder="Color" type="text" id="color" name="color" className="form-control" />
                                            <label htmlFor="color">Color</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleFormChange} value={formData.img_url} placeholder="Color" type="url" id="img_url" name="img_url" className="form-control" />
                                            <label htmlFor="img_url">img_url</label>
                                        </div>
                                    </div>
                                    <button className="btn btn-lg btn-primary">Add Shoe!</button>
                                </form>
                                <div className="alert alert-success d-none mb-0" id="success-message">
                                    Congratulations! Shoe added!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default EditShoeForm

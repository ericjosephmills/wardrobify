import React, { useEffect, useState } from 'react';
import hats from './hats.png'

const EditShoeForm = async (e) => {

    const [bins, setBins] = useState([])
    const url = `http://localhost:8090${e.target.value}`
    console.log(e.target.value)

    const response = await fetch(url)

    const data = await response.json()
    let initialState = {
        fabric: data.fabric,
        style: data.style,
        color: data.color,
        picture: data.picture,
        location: data.location
    }
    const [formData, setFormData] = useState(initialState)


    // const fetchConfigs =
    // {
    //     method: "POST",
    //     body: JSON.stringify(formData),

    //     headers: {
    //         'Content-Type': 'application/json'
    //     }
    // }

    setFormData({
        ...formData,
        [e.target.name]: e.target.value
    })




    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (locations.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    const handleFormChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const shoeUrl = `http://localhost:8090${e.target.value}`
        console.log(formData)
        const fetchConfig =
        {
            method: "PUT",
            body: JSON.stringify(formData),

            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            console.log(response)
            const newHat = await response.json()
            console.log(newHat)
            setFormData(initialState)
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className=" my-5 container">
            <div className="my-5">
                <div className="row">
                    <div className="col col-sm-auto">
                        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src={hats} />
                    </div>
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-hat-form">
                                    <h1 className="card-title">It's hat time!</h1>
                                    <p className="mb-3">
                                        Please choose which location you'd like to put your hat in!
                                    </p>
                                    <div className={spinnerClasses} id="loading-location-spinner">
                                        <div className="spinner-grow text-secondary" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <div className="mb-3">
                                        <select onChange={handleFormChange} value={formData.location} name="location" id="location" className={dropdownClasses} required>
                                            <option value="">Choose a location</option>
                                            {locations.map(location => {
                                                return (
                                                    <option key={location.id} value={location.id}>
                                                        {location.closet_name}
                                                    </option>
                                                )
                                            })}
                                        </select>
                                    </div>
                                    <p className="mb-3">
                                        Now, tell us about the hat!
                                    </p>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleFormChange} value={formData.fabric} required placeholder="Fabric" type="text" id="fabric" name="fabric" className="form-control" />
                                            <label htmlFor="fabric">Fabric</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleFormChange} required value={formData.style} placeholder="Style" type="text" id="style" name="style" className="form-control" />
                                            <label htmlFor="style">Style</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleFormChange} required value={formData.color} placeholder="Color" type="text" id="color" name="color" className="form-control" />
                                            <label htmlFor="color">Color</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleFormChange} value={formData.picture} placeholder="picture" type="url" id="picture" name="picture" className="form-control" />
                                            <label htmlFor="picture">Picture URL</label>
                                        </div>
                                    </div>
                                    <button className="btn btn-lg btn-primary">Add Hat!</button>
                                </form>
                                <div className="alert alert-success d-none mb-0" id="success-message">
                                    Congratulations! Hat added!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default EditShoeForm

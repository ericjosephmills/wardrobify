# Wardrobify

Team:

* Person 1 - Amro on Shoes
* Person 2 - Eric on Hatsgit

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

A poller was made to retrieve data, in the form of binVO's, from the seperate microservice "Wardrobe". These would then populate into the dropdown for bins in the form to add a shoe. The shoe model took that binVO, based on matching ID coming from the json content fetch in order to match the shoe to the bin.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

A program was designed to fetch data in the form of "locationVOs" from a separate microservice called "Wardrobe". This data would then be used to populate a dropdown list of locations in a form used to add a "hat" to the system. The "hat" model would then use the "locationVO", matched by ID, to determine the location of the "hat" being added to the system.
